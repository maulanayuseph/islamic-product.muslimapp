<div class="banner">
    <div class="container">
        <div class="banner-info animated wow zoomIn" data-wow-delay=".5s">
            <div class="wmuSlider example1">
                <div class="wmuSliderWrapper">
                    
                </div>
            </div>
                <script src="{{ asset('template.web/js/jquery.wmuSlider.js') }}"></script> 
                <script>
                    $('.example1').wmuSlider();         
                </script> 
        </div>
    </div>
</div>