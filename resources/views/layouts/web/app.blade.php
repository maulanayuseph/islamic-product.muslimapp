<!DOCTYPE html>
<html>
<head>
<title>MuslimApp | Islamic Product</title>
<link rel="shortcut icon" type="image/png" href="{{ asset('assets/icons/favicon.png') }}"/>

<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->

<link href="{{ asset('template.web/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('template.web/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="{{ asset('template.web/js/jquery.min.js') }}"></script>
<!-- //js -->
<!-- cart -->
<script src="{{ asset('template.web/js/simpleCart.min.js') }}"></script>
<!-- cart -->
<!-- for bootstrap working -->
<script type="text/javascript" src="{{ asset('template.web/js/bootstrap-3.1.1.min.js') }}"></script>
<!-- //for bootstrap working -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

<!-- timer -->
<link rel="stylesheet" href="{{ asset('template.web/css/jquery.countdown.css') }}" />
<!-- //timer -->

<!-- Modernizr JS -->
<script src="{{ asset('template.web/js/modernizr-2.6.2.min.js') }}"></script>

<!-- animation-effect -->
<link href="{{ asset('template.web/css/animate.min.css') }}" rel="stylesheet"> 
<script src="{{ asset('template.web/js/wow.min.js') }}"></script>
<script>
 new WOW().init();
</script>
<!-- //animation-effect -->
</head>
	
<body>
<!-- header -->
	@include('layouts.web.header')
<!-- //header -->

<!-- banner -->
	@include('layouts.web.banner')
<!-- //banner -->

<!-- banner-bottom -->
	@include('layouts.web.banner-bottom')
<!-- //banner-bottom -->

<!-- collections -->
	@include('layouts.web.collections')
<!-- //collections -->

<!-- footer -->
	@include('layouts.web.footer')
<!-- //footer -->
	
</body>
</html>