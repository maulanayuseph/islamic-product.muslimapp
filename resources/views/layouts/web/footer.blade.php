<div class="footer">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-3 footer-grid animated wow" data-wow-delay=".5s">
                <h3>About Us</h3>
                <p>
                    <a href="https://muslimapp.id">MuslimApp.id</a> | Membantu Kebutuhan Umat Muslim dalam Satu Genggaman.
                    <br/>
                    Pengalaman berbelanja berbagai produk muslim Beragam produk muslim ada disini Mulai Sekarang !
                </p>
            </div>
            <div class="col-md-3 footer-grid animated wow" data-wow-delay=".6s">
                <h3>Contact Info</h3>
                <ul>
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Jl. Kolenang No.12 Bandung </li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@muslimapp.id">info@muslimapp.id</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+62 811-2333-724</li>
                </ul>
            </div>
            <div class="col-md-3 footer-grid animated wow" data-wow-delay=".7s">
                <h3>Instagram Posts</h3>
                <div class="footer-grid-left">
                    <a href="https://www.instagram.com/p/BsVEBy8gOXE/?utm_source=ig_embed&amp;utm_medium=loading">
                        <img src="https://scontent-sin2-1.cdninstagram.com/vp/d2e64923a2667462d618264d8397dc70/5CCF67F4/t51.2885-15/sh0.08/e35/s640x640/47581728_161311931514687_7616521900391191947_n.jpg?_nc_ht=scontent-sin2-1.cdninstagram.com&_nc_cat=103 640w" alt=" " class="img-responsive" />
                    </a>
                </div>
                <div class="footer-grid-left">
                    <a href="https://www.instagram.com/p/BsScwFRAMes/?utm_source=ig_embed&amp;utm_medium=loading">
                        <img src="https://scontent-sin2-1.cdninstagram.com/vp/272931205391b3a0ee6736b8a9b83c56/5CCD8327/t51.2885-15/sh0.08/e35/s640x640/47584562_363070011146876_4870311852667377655_n.jpg?_nc_ht=scontent-sin2-1.cdninstagram.com&_nc_cat=108" alt=" " class="img-responsive" />
                    </a>
                </div>
                <div class="footer-grid-left">
                    <a href="https://www.instagram.com/p/BsPup8Bgt9x/?utm_source=ig_embed&amp;utm_medium=loading">
                        <img src="https://scontent-sin2-1.cdninstagram.com/vp/a0a125357f7d1dee8128632c3e631445/5CD8C320/t51.2885-15/sh0.08/e35/s640x640/47582169_225191435050231_6097807595615897429_n.jpg?_nc_ht=scontent-sin2-1.cdninstagram.com&_nc_cat=107" alt=" " class="img-responsive" />
                    </a>
                </div>
                <div class="footer-grid-left">
                    <a href="https://www.instagram.com/p/BsNbyW3AH8r/?utm_source=ig_embed&amp;utm_medium=loading">
                        <img src="https://scontent-sin2-1.cdninstagram.com/vp/b56b044d890f513c2e5b50791fa521a8/5CC23D04/t51.2885-15/sh0.08/e35/s640x640/47693389_371468836949702_1402881881682287241_n.jpg?_nc_ht=scontent-sin2-1.cdninstagram.com&_nc_cat=102" alt=" " class="img-responsive" />
                    </a>
                </div>
                <div class="footer-grid-left">
                    <a href="https://www.instagram.com/p/BsKwp3nAjwq/?utm_source=ig_embed&amp;utm_medium=loading">
                        <img src="https://scontent-sin2-1.cdninstagram.com/vp/bfd8da412cfe476b6b5c113a9235f2b0/5CBDF18F/t51.2885-15/sh0.08/e35/s640x640/47581851_703448160051354_6806130521308529235_n.jpg?_nc_ht=scontent-sin2-1.cdninstagram.com&_nc_cat=106" alt=" " class="img-responsive" />
                    </a>
                </div>
                <div class="footer-grid-left">
                    <a href="https://www.instagram.com/p/BsINCXQgV3j/?utm_source=ig_embed&amp;utm_medium=loading">
                        <img src="https://scontent-sin2-1.cdninstagram.com/vp/8e350479bf0ba0e7200aebecdcfea66e/5CD70057/t51.2885-15/sh0.08/e35/s640x640/49818629_741340592907184_8891263269314554125_n.jpg?_nc_ht=scontent-sin2-1.cdninstagram.com&_nc_cat=104" alt=" " class="img-responsive" />
                    </a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-3 footer-grid animated wow" data-wow-delay=".8s">
                <h3>Blog Posts</h3>
                <div class="footer-grid-sub-grids">
                    <div class="footer-grid-sub-grid-left">
                        <a href="https://news.muslimapp.id/tebuslah-gadai-anak-anda-dengan-aqiqah/">
                            <img src="{{ asset('template.web/images/news1.jpg') }}" alt=" " class="img-responsive" />
                        </a>
                    </div>
                    <div class="footer-grid-sub-grid-right">
                        <h4><a href="https://news.muslimapp.id/tebuslah-gadai-anak-anda-dengan-aqiqah/">Tebuslah Gadai anak anda dengan Aqiqah</a></h4>
                        <p>Posted On 26/10/2018</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-grid-sub-grids">
                    <div class="footer-grid-sub-grid-left">
                        <a href="https://news.muslimapp.id/makna-aqiqah-dan-ketentuannya/">
                            <img src="{{ asset('template.web/images/news2.jpg') }}" alt=" " class="img-responsive" />
                        </a>
                    </div>
                    <div class="footer-grid-sub-grid-right">
                        <h4><a href="https://news.muslimapp.id/makna-aqiqah-dan-ketentuannya/">Makna aqiqah dan ketentuannya</a></h4>
                        <p>Posted On 22/10/2018</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        
        <div class="copy-right animated wow" data-wow-delay=".5s">
            <p>Hak Cipta PT. Digital Muslim Global &copy <?=date("Y")?> </p>
        </div>
    </div>
</div>