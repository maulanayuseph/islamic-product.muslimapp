<div class="header">
    <div class="container">
        <div class="header-grid">
            <div class="header-grid-left animated wow slideInLeft" data-wow-delay=".5s">
                <ul>
                    <!-- <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">IDN <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">ENG</a></li>
                        </ul>
                    </li> -->
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
                        <a href="mailto:info@muslimapp.id">info@muslimapp.id</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+62 811-2333-724</li>
                    <li><i class="glyphicon glyphicon-log-in" aria-hidden="true"></i><a href="login.html">Login</a></li>
                </ul>
            </div>
            <div class="header-grid-right animated wow slideInRight" data-wow-delay=".5s">
                <ul class="social-icons">
                    <li><a href="https://www.facebook.com/muslimapp.id" class="facebook" target="_blank"></a></li>
                    <li><a href="https://www.twitter.com/muslimapp_id" class="twitter" target="_blank"></a></li>
                    <li><a href="https://www.instagram.com/muslimapp.id/" class="instagram" target="_blank"></a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="logo-nav">
            <div class="logo-nav-left animated wow zoomIn" data-wow-delay=".5s">
                <h1>
                    <a href="#">
                        <img src="{{ asset('assets/icons/app_orange_text_icon.png') }}" class="col-lg-9">
                    </a>
                </h1>
            </div>
            <div class="logo-nav-left1">
                <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header nav_2">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.html" class="act">Home</a></li>	
                        <!-- Mega Menu -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Catalog <b class="caret"></b></a>
                            <ul class="dropdown-menu multi-column columns-3">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <ul class="multi-column-dropdown">
                                            <h6>Food</h6>
                                            <li><a href="products.html">Kacang</a></li>
                                            <li><a href="products.html">Coklat</a></li>
                                            <li><a href="products.html">Biskuit</a></li>
                                            <li><a href="products.html">Buah Kering</a></li>
                                            <li><a href="products.html">Madu</a></li>
                                            <li><a href="products.html">Paket Makanan</a></li>
                                            <li><a href="products.html">Kurma</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-4">
                                        <ul class="multi-column-dropdown">
                                            <h6>Non-Food</h6>
                                            <li><a href="products.html">Aneka Perlengkapan Haji</a></li>
                                            <li><a href="products.html">Aneka Perlengkapan Ibadah</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </ul>
                        </li>
                        <li><a href="mail.html">Blog</a></li>
                    </ul>
                </div>
                </nav>
            </div>
            <div class="logo-nav-right">
                <div class="search-box">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your search term..." type="search" id="search">
                            <input class="sb-search-submit" type="submit" value="">
                            <span class="sb-icon-search"> </span>
                        </form>
                    </div>
                </div>
                    <!-- search-scripts -->
                    <script src="{{ asset('template.web/js/classie.js') }}"></script>
                    <script src="{{ asset('template.web/js/uisearch.js') }}"></script>
                        <script>
                            new UISearch( document.getElementById( 'sb-search' ) );
                        </script>
                    <!-- //search-scripts -->
            </div>
            <div class="header-right">
                <div class="cart box_1">
                    <a href="checkout.html">
                        <h3> <div class="total">
                            <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>
                            <img src="{{ asset('template.web/images/bag.png') }}" alt="" />
                        </h3>
                    </a>
                    <p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>
                    <div class="clearfix"> </div>
                </div>	
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>