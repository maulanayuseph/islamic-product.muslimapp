<?php
$nav_mitra = "Mitra";
$nav_order = "Cek Order";
$nav_news = "Berita";

$lptitlelefth1 = "KEBUTUHAN UMAT MUSLIM DALAM SATU APLIKASI";
$lptitlelefth2 = "Muslimapp merupakan aplikasi yang membantu umat muslim untuk melakukan ibadah hanya dalam satu genggaman saja.";

$lpicons = "Aqiqah";
$lpicons2 = "Qurban";
$lpicons3 = "Wedding";
$lpicons4 = "Islamic Products";

$lpfeaturesh1 = "Lebih Banyak Fitur Untuk Beribadahmu";
$lpfeaturesh2 = "Ibadah kamu bakal lebih konsisten dengan beragam fitur dari muslimapp, jalankan kebaikan beribadahmu hanya dalam satu genggaman.";

$Aqiqah = "AQIQAH";
$aqiqahh5 = "Fitur Layanan Aqiqah online berbasis mobile aplikasi pertama di Indonesia, keperluan Aqiqah kamu bakal lebih mudah, cepat, dan menyenangkan hanya dengan lewat aplikasi yang kami hadirkan untuk anda.";
$getnow = "Order Sekarang";

$Qurban = "QURBAN";
$qurbanh5 = "Fitur Layanan Qurban Online yang memudahkan kamu untuk menunaikan
ibadah kurban lebih praktis dan cepat.
";
$qurbangn = "Qurban Sekarang";

$Wedding = "WEDDING";
$weddingh5 = "Fitur Layanan untuk keperluan pernikahan kamu dengan banyak pilihan paket
lengkap dan menarik, konsultasi dengan kami dan pilih sesuai dengan
kebutuhanmu.
";
$weddinggn = "Dapatkan Sekarang";

$islamic_product = "ISLAMIC PRODUCT";
$islamic_product_desc = "Pengalaman berbelanja berbagai produk muslim Beragam produk muslim ada disini Mulai Sekarang.";
$getnow_islamicproduct = "Mulai";

$feature = "FITUR KAMI";
$quran = "Al-Qur'an";
$ah = "Asma'ul Husna";
$aq = "Aqiqah";
$kal = "Kalender";
$qur = "Qurban";
$js = "Jadwal Shalat";
$k = "Kiblat";
$m = "Masjid";
$as = "Al -Matsurat";
$s = "Syahadat";
$d = "Do'a - Do'a";
$t = "Tasbih";
$kh = "Kumpulan Hadits";
$lm = "Live Mekkah";
$v = "Voucher";
$ip = "Produk Islami";
$gps = "GPS Lacak Haji Dan Umroh";

$tes1 = "Nice banget pokoknya. Aku minta dianter jam 10 dan on time banget. Makanannya juga enak. Praktis, mudah, no ribet.";
$tes2 = "Service atau pelayanan sangat baik, mulai dari informasi, konfirmasi  dilayani, dibantu  dengan baik, ramah.
        Paket aqiqah datang tepat waktu, kemasannya, bahannya dan desainnya bagus serta berkelas, olahan menu masakan baik dan rasanya tidak mengecewakan.
        Kami akan merekomendasikan layanan paket aqiqah dari muslimapp kepada yang lain.";
$tes3 = "Kemasan menarik, datang tepat waktu, pelayanan dari pemesanan hingga pengantaran sangat baik serta rasa masakannya enak. untuk masukan bisa ditambahkan sambal.";

$gt = "Dapatkan Sekarang!";

$foot1 = "Syarat dan Ketentuan";
$foot2 = "Kebijakan dan Privasi";
$foot3 = "Hubungi Kami";

$cp = "Hak Cipta PT. Digital Muslim Global &copy; 2018";

$con = "Kontak Whatsapp";

?>